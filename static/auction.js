const ws_lot = new WebSocket('ws://' + window.location.host + '/ws_lot');
const ws_main = new WebSocket('ws://' + window.location.host + '/ws_main');

const main = 'main';
const main_update = 'main_update';
const main_close_lot = 'main_close_lot';
const status_opened = 'opened';
const status_closed = 'closed';
const splited_href = window.location.href.split('/');

let auth_status = $('#auth_status').text();
let href_status = "isDisabled";

ws_main.onmessage = function (event) {
    splited_data = event.data.split(" ");
    let lot_id = '#' + splited_data[1];

    if (splited_data[0] == main) {
        if (auth_status == "logged") {
            href_status = "";
        }
        let lot_box = '<a href="/lot/'+ splited_data[1] +'" class="alert alert-warning ' + href_status + '" id="'
        + splited_data[1] + '" role="alert">Lot: ' + splited_data[2] + ', Start Price: ' + splited_data[3]+ '</a>';

        $('#lot-list').append(lot_box);
        $('#new_lot_name').val('');
        $('#new_lot_start_price').val('');

    } else if (splited_data[0] == main_update) {

        $(lot_id).text("Lot: " + splited_data[2] + " Start Price: "
        + splited_data[3] + ", Price Up: " + splited_data[4] + " by user: " + splited_data[5]);

    } else if (splited_data[0] == main_close_lot) {
        elem = $(lot_id);
        elem.text("CLOSED");
        elem.removeAttr('href');
    };
};

ws_lot.onmessage = function (event) {
    splited_data = event.data.split(" ");

    if (splited_data[0] == 'opened') {
        let lot_price_up = $('#lot_price_up');
        if (!lot_price_up.text()) {
            let price_up = '<div class="row"><div class="col-md-2"><div>Price Up: </div></div><div class="col-md-1"><div id="lot_price_up">'
            + splited_data[3] + '</div></div><div class="col-md-1"><div>by user: </div></div><div class="col-md-1"><div id="by_user">'
            + splited_data[4] + '</div></div></div>'
            $('.lot-window').append(price_up);
        } else {
            lot_price_up.text(splited_data[3]);
            $('#by_user').text(splited_data[4]);
        };
        const data = {page: main_update, lot_id: splited_href[splited_href.length - 1], lot_name: splited_data[1],
            lot_start_price: splited_data[2], price_up: splited_data[3], by_user: splited_data[4]};

        ws_main.send(JSON.stringify(data));

    } else if (splited_data[0] == 'closed') {
        let status_closed = '<div>Status: CLOSED</div>';
        $('.lot-window').append(status_closed);
        $('#close_auction_btn, #up, #price_up').attr('disabled','disabled');
        const data = {page: main_close_lot, lot_id: splited_href[splited_href.length - 1]};

        ws_main.send(JSON.stringify(data));
    };
};

$('#up').click(function () {
    let lot_name = $('#lot_name').text();
    let lot_start_price = $('#lot_start_price').text();
    let lot_price_up = parseInt($('#lot_price_up').text());
    let price_up = $('#price_up').val();

    if (lot_price_up && price_up > lot_price_up || !lot_price_up && price_up > parseInt(lot_start_price)) {
        fetch('/lot', {
            method: 'POST',
            body: JSON.stringify({status: status_opened, price_up: price_up}),
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(async res => {
            const splited_href = window.location.href.split('/');
            const data = {status: status_opened, lot_id: splited_href[splited_href.length - 1], lot_name: lot_name,
                lot_start_price: lot_start_price, price_up: price_up};
            $('#price_up').val("");

            ws_lot.send(JSON.stringify(data));
            });
    } else alert('Price Up < Current Price');
});

$('#add_lot_btn').click(function () {
    let lot_name = $('#new_lot_name').val();
    lot_name=lot_name.replace(/\s/ig, '_');
    const lot_start_price = $('#new_lot_start_price').val();
    if (lot_name && lot_start_price) {
        fetch('/create/lot', {
            method: 'POST',
            body: JSON.stringify({name: lot_name, lot_start_price: lot_start_price}),
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(async res => {
            const data = await res.json();
            ws_main.send(JSON.stringify({page: main, lot_id: data.id, lot_name: lot_name, lot_start_price: lot_start_price}));
        });
    };
});

$('#close_auction_btn').click(function () {
    fetch('/lot', {
            method: 'POST',
            body: JSON.stringify({status: 'closed'}),
            headers: {
                'Content-Type': 'application/json'
            }
    }).then(async res => {
        ws_lot.send(JSON.stringify({status: status_closed, lot_id: splited_href[splited_href.length - 1]}));
    });
});
