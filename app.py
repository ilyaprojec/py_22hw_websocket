import aioreloader
import aiohttp_jinja2
import motor.motor_asyncio
from pymongo import MongoClient
from jinja2 import FileSystemLoader
from aiohttp import web
from aiohttp_session import setup
from aiohttp_session import SimpleCookieStorage
from handlers import Index, LotWindow, WebSocketLot, WebSocketMain, Registration, Login, Lot

if __name__ == '__main__':
    app = web.Application()

    app.ws_lot_list = []
    app.ws_main_list = []

    client = motor.motor_asyncio.AsyncIOMotorClient('mongodb://root:example123@localhost:27017/')
    sync_client = MongoClient('mongodb://root:example123@localhost:27017/')
    app.db = client['auction']
    app.sync_db = sync_client['auction']

    aiohttp_jinja2.setup(app, loader=FileSystemLoader('templates'))
    setup(app, SimpleCookieStorage())

    app.router.add_static('/static/', 'static')

    app.router.add_get('/', Index)
    app.router.add_post('/', Index)

    app.router.add_get('/lot/{lot_id:[a-fA-F0-9]{24}}', LotWindow)
    app.router.add_post('/lot', LotWindow)

    app.router.add_get('/registration', Registration)
    app.router.add_post('/registration', Registration)

    app.router.add_get('/login', Login)
    app.router.add_post('/login', Login)

    app.router.add_get('/ws_lot', WebSocketLot)
    app.router.add_get('/ws_main', WebSocketMain)

    app.router.add_post('/create/lot', Lot)

    aioreloader.start()
    web.run_app(app, port=8888)
