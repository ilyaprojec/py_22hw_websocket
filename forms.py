import base64
from wtforms import Form, ValidationError
from wtforms import StringField, PasswordField
from wtforms.validators import DataRequired
from wtforms.validators import Regexp


class DbMixin:

    def __init__(self, *args, **kwargs):
        self.db = kwargs.pop('db')
        super(DbMixin, self).__init__(*args, **kwargs)


class RegistrationForm(DbMixin, Form):
    username = StringField('username', [Regexp(r'^[\w.@+-]+$'), DataRequired()])
    password = PasswordField('password', validators=[DataRequired()])
    password_confirm = PasswordField('password_confirm', validators=[DataRequired()])

    def validate_username(self, field):
        result = self.db['users'].find_one({'username': field.data})

        if result:
            raise ValidationError('This user already exists')

    def validate_password_confirm(self, field):
        if self.password.data != field.data:
            raise ValidationError('Passwords Should Match')

    async def save(self, db):
        await db['users'].insert_one({
            'username': self.username.data,
            'password': base64.b16encode(bytes(self.password.data, 'utf-8'))
        })
        return self.username.data


class LoginForm(DbMixin, Form):
    username = StringField('username', validators=[DataRequired()])
    password = PasswordField('password', validators=[DataRequired()])

    def validate_password(self, field):
        pwd = base64.b16encode(bytes(self.password.data, 'utf-8'))
        result = self.db['users'].find_one({'username': self.username.data, 'password': pwd})
        if not result:
            raise ValidationError('Password or username is incorrect')
