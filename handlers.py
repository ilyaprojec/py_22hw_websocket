import json
import aiohttp_jinja2
from aiohttp.web_exceptions import HTTPNotFound
from aiohttp_session import get_session
from aiohttp.web import View, WebSocketResponse, json_response
from forms import RegistrationForm, LoginForm
from bson import ObjectId


class Index(View):

    @aiohttp_jinja2.template('main.html')
    async def get(self):
        lots = await self.request.app.db['lots'].find({"status": "opened"}).to_list(length=100)
        session = await get_session(self.request)

        if session.get('username'):
            return {'lots': lots}
        else:
            return {'lots': lots, 'disabled': 'disabled'}


class Registration(View):

    @aiohttp_jinja2.template('registration.html')
    def get(self):

        return {
            'form': RegistrationForm(db=self.request.app.sync_db),
            'redirect': False
        }

    @aiohttp_jinja2.template('registration.html')
    async def post(self):
        form = RegistrationForm(
            formdata=await self.request.post(),
            db=self.request.app.sync_db
        )

        if form.validate():
            username = await form.save(self.request.app.db)
            session = await get_session(self.request)
            session['username'] = username

            return {'redirect': True, 'form': form}

        return {
            'form': form,
            'redirect': False
        }


class Login(View):

    @aiohttp_jinja2.template('login.html')
    def get(self):

        return {'form': LoginForm(db=self.request.app.sync_db)}

    @aiohttp_jinja2.template('login.html')
    async def post(self):
        form = LoginForm(
            formdata=await self.request.post(),
            db=self.request.app.sync_db
        )

        if form.validate():
            session = await get_session(self.request)
            session['username'] = str(form.username.data)

            return {'redirect': True, 'form': form}

        return {
            'form': form,
            'redirect': False
        }


class LotWindow(View):

    @aiohttp_jinja2.template('lot.html')
    async def get(self):
        session = await get_session(self.request)
        if not session.get('username'):
            raise HTTPNotFound

        session['lot_id'] = self.request.match_info['lot_id']
        lot = await self.request.app.db['lots'].find_one({'_id': ObjectId(session['lot_id'])})
        return {'lot': lot, 'creator': session['username']}

    async def post(self):
        session = await get_session(self.request)
        data = await self.request.json()

        if data['status'] == 'opened':
            await self.request.app.db['lots'] .update_one({'_id': ObjectId(session['lot_id'])}, {"$set": {"price_up": data['price_up'], "by_user": session['username']}})
            return json_response({})

        elif data['status'] == 'closed':
            await self.request.app.db['lots'].update_one({'_id': ObjectId(session['lot_id'])}, {"$set": {"status": "closed"}})
            return json_response({})


class Lot(View):

   async def post(self):
        session = await get_session(self.request)
        data = await self.request.json()
        result = await self.request.app.db['lots'].insert_one({
            'name': data['name'],
            'start_price': data['lot_start_price'],
            'creator': session['username'],
            'status': 'opened'
        })

        return json_response({
            'id': str(result.inserted_id)
        })


class WebSocketMain(View):

    async def get(self):
        ws = WebSocketResponse()
        await ws.prepare(self.request)
        self.request.app.ws_main_list.append(ws)

        async for message in ws:
            data = json.loads(message.data)

            for ws_con in self.request.app.ws_main_list:
                if data['page'] == 'main':
                    await ws_con.send_str(f"{data['page']} {data['lot_id']} {data['lot_name']} {data['lot_start_price']}")
                elif data['page'] == 'main_update':
                    await ws_con.send_str(f"{data['page']} {data['lot_id']} {data['lot_name']} {data['lot_start_price']} {data['price_up']} {data['by_user']}")
                elif data['page'] == 'main_close_lot':
                    await ws_con.send_str(f"{data['page']} {data['lot_id']}")

        self.request.app.ws_main_list.remove(ws)
        return ws


class WebSocketLot(View):

    async def get(self):
        ws = WebSocketResponse()
        await ws.prepare(self.request)
        session = await get_session(self.request)
        ws.lot_id = session['lot_id']
        self.request.app.ws_lot_list.append(ws)

        async for message in ws:
            data = json.loads(message.data)

            for ws_con in self.request.app.ws_lot_list:
                if ws_con.lot_id == data['lot_id']:
                    if data['status'] == 'opened':
                        await ws_con.send_str(f"{data['status']} {data['lot_name']} {data['lot_start_price']} {data['price_up']} {session['username']}")
                    elif data['status'] == 'closed':
                        await ws_con.send_str(f"{data['status']}")

        self.request.app.ws_lot_list.remove(ws)
        return ws
